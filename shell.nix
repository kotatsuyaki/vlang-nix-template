{ pkgs ? import <nixpkgs> { } }:

let
  vlang = import ./vlang.nix {};
  vls = import ./vls.nix { vlang = vlang; };
in
  pkgs.mkShell {
    buildInputs = [ vlang vls ];
    VROOT = "${vlang}/lib";
  }
